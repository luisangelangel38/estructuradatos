/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package modelo;
//dato= aux.setinfo();

import controlador.ListaEnlazada;
import controlador.NodoLista;

/**
 *
 * @author luisquirola
 */
public class Sucursal<E> {

    private int id;
    private String nombre;

    private ListaEnlazada<Venta> ventas;

    public ListaEnlazada<Venta> getVentas() {
        return ventas;
    }

    public void setVentas(ListaEnlazada<Venta> ventas) {
        this.ventas = ventas;
    }
    private NodoLista<E> cabeceraVentas;

    public NodoLista<E> getCabeceraVentas() {
        return cabeceraVentas;
    }

    public void setCabeceraVentas(NodoLista<E> cabeceraVentas) {
        this.cabeceraVentas = cabeceraVentas;
    }

    private int size;

    public int getId() {
        return id;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

}
