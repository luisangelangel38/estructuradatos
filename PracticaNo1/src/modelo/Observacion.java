/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package modelo;

import java.util.Date;

/**
 *
 * @author luisquirola
 */
public class Observacion {

    private String reclamo;
    private Date fecha;

    public Observacion(String reclamo, Date fecha) {
        this.reclamo = reclamo;
        this.fecha = fecha;
    }

    public Observacion() {
    }
    

    public String getReclamo() {
        return reclamo;
    }

    public void setReclamo(String reclamo) {
        this.reclamo = reclamo;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

}
