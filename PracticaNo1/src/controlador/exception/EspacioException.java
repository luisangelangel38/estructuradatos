/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controlador.exception;

/**
 *
 * @author luisquirola
 */
public class EspacioException extends Exception {

    public EspacioException() {
        super("Espacio Lleno o posicion no valida");
    }

    public EspacioException(String message) {
        super(message);
    }
}
