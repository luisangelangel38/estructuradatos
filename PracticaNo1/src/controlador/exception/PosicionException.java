/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controlador.exception;

/**
 *
 * @author luisquirola
 */
public class PosicionException extends Exception {

    public PosicionException() {
        super("No existe la posicion en la lista");
    }

    public PosicionException(String msg) {
        super(msg);
    }
}
