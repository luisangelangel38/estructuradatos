/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controlador.pila;

import controlador.exception.EmptyException;
import controlador.exception.PosicionException;
import controlador.exception.TopeException;

/**
 *
 * @author luisquirola
 */
public class Pila <E>{
     private PilaI<E> pilai;

    public Pila(Integer cima) {
        pilai = new PilaI<>(cima);
    } 

    public void push(E obj) throws TopeException  {
        pilai.push(obj);
    }
    
    public E pop() throws EmptyException, PosicionException {
        return pilai.pop();
    }
    
    public Integer getCima() {
        return pilai.getCima();
    }
    
    public void print() throws EmptyException  {
        pilai.imprimir();
    }
}
