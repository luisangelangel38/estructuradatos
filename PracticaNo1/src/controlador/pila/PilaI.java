/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controlador.pila;

import controlador.ListaEnlazada;
import controlador.exception.EmptyException;
import controlador.exception.PosicionException;
import controlador.exception.TopeException;

/**
 *
 * @author luisquirola
 */
public class PilaI<E> extends ListaEnlazada<E> {

    private Integer cima;

    public PilaI(Integer cima) {
        this.cima = cima;
    }

    public Boolean isFull() {
        return (size() >= cima);
    }

    
    public void push(E info) throws TopeException {
        if (!isFull()) {
            insertarInicio(info);
        } else {
            throw new TopeException();
        }
    }

    
    public E pop() throws EmptyException, PosicionException {
        E dato = null;
        if (isEmpty()) {
            throw new EmptyException("Pila vacia");
        } else {
            return this.delete(0);
        }
    }

    public Integer getCima() {
        return cima;
    }

}
