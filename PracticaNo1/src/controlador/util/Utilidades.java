/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controlador.util;

import controlador.DAO.AdaptadorDao;
import controlador.ListaEnlazada;
import controlador.NodoLista;
import modelo.Sucursal;
import modelo.Venta;

/**
 *
 * @author luisquirola
 */
public class Utilidades {

    AdaptadorDao dao;

    public  static Double sumarVentas(Sucursal s) {
        Double suma = 0.0;
        ListaEnlazada<Venta> ventas = s.getVentas();
        NodoLista<Venta> nodo = ventas.getCabecera();
        while (nodo != null) {
            Venta venta1 = nodo.getInfo();
            suma = suma + venta1.getValor();
            nodo = nodo.getSig();
        }

        return suma;

    }

    public static Double mediaVentas(Sucursal s) {
        Double suma = sumarVentas(s);
        ListaEnlazada<Venta> ventas = s.getVentas();
        if (suma == 0) {
            return suma;
        } else {
            return suma / ventas.size();
        }

    }

}
