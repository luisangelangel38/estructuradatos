/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controlador;

import modelo.EnumMes;
import modelo.Sucursal;
import modelo.Venta;

/**
 *
 * @author luisquirola
 */
public class SucursalControlador {

    private ListaEnlazada<Sucursal> sucursales;
    private Sucursal sucursal;
    private Venta venta;
    private ListaEnlazada<Venta> ventas;

    public ListaEnlazada<Venta> getVentas() {

        return ventas;
    }

    public void setVentas(ListaEnlazada<Venta> ventas) {
        this.ventas = ventas;
    }
    private int id = 0;

    public SucursalControlador() {
        sucursales = new ListaEnlazada<>();
        ventas = new ListaEnlazada<>();
//        if (ventas.isEmpty()) {
//            this.ventas = new ListaEnlazada<>();
//            for (EnumMes mes : EnumMes.values()) {
//                this.venta = new Venta();
//                getVenta().setMes(mes);
//                getVenta().setValor(0.0);
//                getVenta().setId(id);
//                id++;
//                ventas.agregar(venta);
//            }
//        }
    }

    public SucursalControlador(ListaEnlazada<Sucursal> sucursales, Sucursal sucursal, Venta venta, ListaEnlazada<Venta> ventas) {
        this.sucursales = sucursales;
        this.sucursal = sucursal;
        this.venta = venta;
        this.ventas = ventas;
    }
    

//    public boolean guardarVentas(Integer posVenta, Double valor) throws EspacioException, EmptyException, PosicionException {
//        if (this.sucursal != null) {
//            if (posVenta <= this.sucursal.getVentas().size() - 1) {
//                //sucursal.getVentas()[posVenta].setValor(valor);
//                sucursal.getVentas().get(posVenta).setValor(valor);
//            } else {
//                throw new EspacioException();
//            }
//        } else {
//            throw new NullPointerException("Debe seleccionar una sucursal: ");
//        }
//
//        return true;
//    }
    public Venta getVenta() {
//        if (venta == null) {
//            venta = new Venta();
//        }
        return venta;
    }

    public void setVenta(Venta venta) {
        this.venta = venta;
    }

    public Sucursal getSucursal() {
//        if (sucursal == null) {
//            sucursal = new Sucursal();
//        }
        return sucursal;
    }

    public void setSucursal(Sucursal sucursal) {
        this.sucursal = sucursal;
    }

    public ListaEnlazada<Sucursal> getSucursales() {
        return sucursales;
    }

    /**
     * @param sucursales the sucursales to set
     */
    public void setSucursales(ListaEnlazada<Sucursal> sucursales) {
        this.sucursales = sucursales;
    }

}
