/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package controlador.DAO;

import controlador.cola.Cola;
import controlador.exception.EmptyException;
import controlador.exception.PosicionException;
import controlador.exception.TopeException;

/**
 *
 * @author luisquirola
 */
public interface InterfazDaoCola <T>{
     public void guardar(T obj)throws TopeException;
    public void modificar(T obj, Integer pos);
    public T obtener(Integer id);
    public Cola<T> listar();
    public void eliminar() throws EmptyException, PosicionException;
}
