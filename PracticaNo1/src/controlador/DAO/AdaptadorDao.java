/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controlador.DAO;

import controlador.ListaEnlazada;
import controlador.exception.EmptyException;
import controlador.exception.PosicionException;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author luisquirola
 */
public class AdaptadorDao<T> implements InterfazDao {

    private Conexion conexion;
    private Class clazz;
    private String url;

    public AdaptadorDao(Class clazz) {
        this.conexion = new Conexion();
        this.clazz = clazz;
        this.url = Conexion.url + clazz.getSimpleName().toLowerCase() + ".json";
    }

    @Override
    public void guardar(Object obj) {
        try {
            ListaEnlazada<T> lista = listar();
            lista.agregar((T) obj);
            conexion.getXstream().alias(lista.getClass().getName(), ListaEnlazada.class);
            conexion.getXstream().toXML(lista, new FileWriter(url));
        } catch (IOException ex) {
            Logger.getLogger(AdaptadorDao.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public T obtener(Integer id) {
        ListaEnlazada<T> lista = listar();
        return (T) lista;
    }

    @Override
    public ListaEnlazada<T> listar() {
        ListaEnlazada<T> lista = new ListaEnlazada<>();

        try {
            lista = (ListaEnlazada<T>) conexion.getXstream().fromXML(new File(url));
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
        return lista;
    }

    @Override
    public void modificar(Object obj, Integer pos) {
        try {
            ListaEnlazada<T> lista = listar();

            lista.update(pos, (T) obj);
            conexion.getXstream().alias(lista.getClass().getName(), ListaEnlazada.class);
            conexion.getXstream().toXML(lista, new FileWriter(url));
        } catch (EmptyException ex) {
            Logger.getLogger(AdaptadorDao.class.getName()).log(Level.SEVERE, null, ex);
        } catch (PosicionException ex) {
            Logger.getLogger(AdaptadorDao.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(AdaptadorDao.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
}
