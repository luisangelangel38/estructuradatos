/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controlador.DAO;

import controlador.ListaEnlazada;
import java.io.IOException;

/**
 *
 * @author luisquirola
 */
public interface InterfazDao <T> {
    public void guardar(T obj)throws IOException;
    public void modificar(T obj, Integer pos);
    public T obtener(Integer id);
    public ListaEnlazada<T> listar();
}
