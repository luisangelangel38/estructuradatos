/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controlador.cola;

import controlador.exception.EmptyException;
import controlador.exception.PosicionException;
import controlador.exception.TopeException;

/**
 *
 * @author luisquirola
 */
public class Cola<E> {

    public Colai<E> colaI;

    public Cola(Integer tope) {
        colaI = new Colai<>(tope);
    }

    public void queue(E obj) throws TopeException {
        colaI.queue(obj);
    }

    public E dequeue() throws EmptyException, PosicionException {
        return colaI.dequeue();
    }

    public Integer getTope() {
        return colaI.getTope();
    }

    public void imprimir() throws EmptyException {
        colaI.imprimir();
    }


    public Colai<E> getCola() {
        return colaI;
    }

    public void setCola(Colai<E> colaI) {
        this.colaI = colaI;
    }
}
