/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controlador.cola;

import controlador.ListaEnlazada;
import controlador.exception.EmptyException;
import controlador.exception.PosicionException;
import controlador.exception.TopeException;

/**
 *
 * @author luisquirola
 */
public class Colai <E> extends ListaEnlazada<E>{
    private Integer tope;

    public Colai() {
    }

    public Colai(Integer tope) {
        this.tope = tope;
    }
    
    public Boolean estaLleno(){
        return (size() >= tope); 
    }
    
    public void queue(E dato) throws TopeException{
        if (estaLleno()) 
            throw new TopeException("ESTA LLENO");
        
        else 
            this.agregar(dato);

    }
    
    public E dequeue() throws EmptyException, PosicionException{
        if(isEmpty()){
            throw new EmptyException("Cola vacia");
        }
        else {
            return this.delete(0);
        }
    }

    /**
     * @return the tope
     */
    public Integer getTope() {
        return tope;
    }

    /**
     * @param tope the tope to set
     */
    public void setTope(Integer tope) {
        this.tope = tope;
    }
}
