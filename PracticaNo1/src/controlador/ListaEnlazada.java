/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controlador;

import controlador.exception.EmptyException;
import controlador.exception.EspacioException;
import controlador.exception.PosicionException;
import modelo.Sucursal;
import modelo.Venta;

/**
 *
 * @author luisquirola
 */
public class ListaEnlazada<E> {

    private NodoLista<E> cabecera;
    private Integer size = 0;

    private Venta ventas;

    public boolean isEmpty() {
        return cabecera == null;
    }

    public void agregar(E e) {
        NodoLista<E> insercion = new NodoLista<>(e, null);

        if (isEmpty()) {
            this.cabecera = insercion;
            this.size++;
        } else {
            NodoLista<E> aux = cabecera;
            while (aux.getSig() != null) {
                aux = aux.getSig();
            }
            aux.setSig(insercion);
            this.size++;
        }
    }

////    public void agregarInicio(E info) {
////        if (isEmpty()) {
////            agregar(info);
////        } else {
////            NodoLista<Sucursal> insercion = new NodoLista<>(info, null);
////            insercion.setSig(cabecera);
////            cabecera = insercion;
////            size++;
////        }
////    }
    public void insertarInicio(E info) {

        if (isEmpty()) {
            agregar(info);
        } else {
            NodoLista<E> nuevo = new NodoLista<>(info, null);
            nuevo.setSig(cabecera);
            cabecera = nuevo;
            size++;
        }
    }

    public void insertarPosicion(E info, Integer pos) throws PosicionException {
        if (isEmpty()) {
            agregar(info);
        } else if (pos >= 0 && pos < size()) {
            insertarInicio(info);

        } else if (pos >= 0 && pos < size()) {
            NodoLista<E> nuevo = new NodoLista<>(info, null);
            NodoLista<E> aux = cabecera;
            for (int i = 0; i < (pos - 1); i++) {
                aux = aux.getSig();
            }
            NodoLista<E> sig = aux.getSig();
            aux.setSig(nuevo);
            nuevo.setSig(sig);
        } else {
            throw new PosicionException();
        }
    }

    public E get(Integer pos) throws EmptyException, PosicionException {
        if (isEmpty()) {
            throw new EmptyException();
        } else {
            E dato = null;
            if (pos >= 0 && pos < size()) {
                if (pos == 0) {
                    dato = cabecera.getInfo();
                } else {
                    NodoLista<E> aux = cabecera;
                    for (int i = 0; i < pos; i++) {
                        aux = aux.getSig();
                    }
                    dato = aux.getInfo();
                }
            } else {
                throw new PosicionException();
            }

            return dato;
        }

    }

    public E delete(Integer pos) throws EmptyException, PosicionException {
        if (isEmpty()) {
            throw new EmptyException();
        } else {
            E dato = null;
            if (pos >= 0 && pos < size()) {
                if (pos == 0) {
                    dato = cabecera.getInfo();
                    cabecera = cabecera.getSig();
                    size--;
                } else {
                    NodoLista<E> aux = cabecera;
                    for (int i = 0; i < pos - 1; i++) {
                        aux = aux.getSig();
                    }
                    dato = aux.getInfo();

                    if (pos + 1 == size()) {
                        NodoLista<E> aux1 = aux.getSig();
                        dato = aux1.getInfo();
                    } else {
//                        NodoLista<E> proximo = aux.getSig();
//                        aux.setSig(proximo.getSig());
//                        size--;
                    }
                    NodoLista<E> proximo = aux.getSig();
                    aux.setSig(proximo.getSig());
                    size--;
                }
            } else {
                throw new PosicionException();
            }

            return dato;
        }

    }

    public NodoLista<E> getCabecera() {
        return cabecera;
    }

    public void setCabecera(NodoLista<E> cabeceraSucursales) {
        this.cabecera = cabecera;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public Venta getVentas() {
        return ventas;
    }

    public void setVentas(Venta ventas) {
        this.ventas = ventas;
    }

    public boolean guardar() {

        return true;
    }

    /**
     * @return the cabeceraVentas
     */
    public Integer size() {
        return size;
    }

    public void imprimir() throws EmptyException {
        if (isEmpty()) {
            throw new EmptyException();
        } else {
            NodoLista<E> aux = cabecera;
            System.out.println("----Inicio Lista-----");
            for (int i = 0; i < size(); i++) {
                System.out.println(aux.getInfo());
                aux = aux.getSig();
            }
            System.out.println("-------Fin Lista-------");
        }

    }

    public Double sumarVentas(ListaEnlazada<Venta> lista) {
        Double suma = 0.0;
//        NodoLista<E> aux = cabeceraVentas;
//        while (aux.getSig() != null) {
//            suma = aux.getSig() + suma;
//
//        }
//        for (Venta v : lista) {
//            suma = aux.getSig() + suma;
//        }
        return suma;

    }

    public void modificar(E info, Integer pos) throws PosicionException {
        NodoLista<E> aux = cabecera;
        int indice = 0;
        while (aux != null) {
            if (indice == pos) {
                aux.setInfo(info);
                break;
            }
            aux = aux.getSig();
            indice++;
        }

    }

    public void update(Integer pos, E dato) throws EmptyException, PosicionException {
        if (isEmpty()) {
            throw new EmptyException();
        } else {

            if (pos >= 0 && pos < size()) {
                if (pos == 0) {
                    //dato = cabecera.getInfo();
                    cabecera.setInfo(dato);
                } else {
                    NodoLista<E> aux = cabecera;
                    for (int i = 0; i < pos; i++) {
                        aux = aux.getSig();
                    }
                    aux.setInfo(dato);
                }
            } else {
                throw new PosicionException();
            }
            //return dato;
        }
    }

}
