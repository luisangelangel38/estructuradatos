/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package vista.modeloTabla;

import controlador.cola.Cola;
import controlador.exception.EmptyException;
import controlador.exception.PosicionException;
import javax.swing.table.AbstractTableModel;
import modelo.Historial;

/**
 *
 * @author luisquirola
 */
public class ModeloTablaHistorial extends AbstractTableModel {

    private Cola<Historial> cola = new Cola<>(20);

    @Override
    public int getRowCount() {
        return cola.getCola().size();
    }

    @Override
    public int getColumnCount() {
        return 3;
    }

    @Override
    public Object getValueAt(int i, int i1) {
        try {
            Historial historial = cola.getCola().get(i);
            switch (i1) {
                case 0:
                    return (historial != null) ? historial.getFecha().getDate() : "NO DEFINIR";
                case 1:
                    return (historial != null) ? historial.getVenta().getMes().name() : "NO DEFINIR";
                case 2:
                    return (historial != null) ? historial.getVenta().getValor() : "NO DEFINIR";
                default:
                    return null;
            }
        } catch (EmptyException ex) {
            System.err.println(ex.getMessage());
        } catch (PosicionException ex) {
            System.err.println(ex.getMessage());
        }
        return null;
    }

    @Override
    public String getColumnName(int column) {
        switch (column) {
            case 0:
                return "FECHA DEL CAMBIO";
            case 1:
                return "MES";
            case 2:
                return "VALOR ACTUALIZADO";
            default:
                return null;
        }
    }

    public Cola<Historial> getCola() {
        return cola;
    }

    public void setCola(Cola<Historial> cola) {
        this.cola = cola;
    }
}
