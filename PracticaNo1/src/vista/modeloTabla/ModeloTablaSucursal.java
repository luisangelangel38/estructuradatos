/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package vista.modeloTabla;

import controlador.ListaEnlazada;
import controlador.exception.EmptyException;
import controlador.exception.PosicionException;
import controlador.util.Utilidades;
import javax.swing.table.AbstractTableModel;
import modelo.Sucursal;

/**
 *
 * @author luisquirola
 */
public class ModeloTablaSucursal extends AbstractTableModel {

    private ListaEnlazada<Sucursal> sucursal = new ListaEnlazada<>();

    public int getRowCount() {
        return getSucursal().size();
    }

    public int getColumnCount() {
        return 3;
    }

    public Object getValueAt(int i, int i1) {
        Sucursal s1;
        try {
            s1 = getSucursal().get(i);
            switch (i1) {
                case 0:
                    return (s1 != null) ? s1.getNombre() : "NO DEFINIR";
                case 1:
                    return (s1 != null) ? Utilidades.sumarVentas(s1) : 0.0;
                case 2:
                    return (s1 != null) ? Utilidades.mediaVentas(s1) : 0.0;
                default:
                    return null;
            }
        } catch (EmptyException | PosicionException ex) {
            System.out.println(ex.getMessage());
        }
        return null;

    }

    public String getColumnName(int column) {
        switch (column) {
            case 0:
                return "SUCURSAL";
            case 1:
                return "VENTA";
            case 2:
                return "PROMEDIO VENTA";
            default:
                return null;
        }

    }

    public ListaEnlazada<Sucursal> getSucursal() {
        return sucursal;
    }

    public void setSucursal(ListaEnlazada<Sucursal> sucursal) {
        this.sucursal = sucursal;
    }
}
