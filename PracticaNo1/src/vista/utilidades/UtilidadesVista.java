/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package vista.utilidades;

import controlador.pila.Pila;
import java.io.FileWriter;
import java.util.Date;

/**
 *
 * @author luisquirola
 */
public class UtilidadesVista {

    public void guardarHistorial(Pila<String> pila) {
        FileWriter archivo = null;
        String s = "";
        try {
            archivo = new FileWriter("data/historial.txt");
            String aux = pila.pop();
            try {
                s += aux + "FECHA: " + new Date() + "\n";
                while (aux != null) {
                    aux = pila.pop();
                    s += aux + "FECHA:  " + new Date() + "\n";

                }
            } catch (Exception e) {
            }

            archivo.write(s.toString());
            archivo.close();
        } catch (Exception e) {
            System.out.println("ERROR EN HISTORIAL" + e);
        }

    }
}
